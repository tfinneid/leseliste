


function opprettMappe() {

    let mappe = document.getElementById("mappeInput").value

    mappeUrl = `/api/folders/${mappe}`

    fetch(`${mappeUrl}`, {
        method: 'POST'
    })
    .then(response => { if (! response.ok) alert("Feil 23: Kunne ikke opprette mappe: " + mappe) } )
    .then(result => { window.location.reload() } )
    .catch(error => { alert(`Opprettelse feilet\n${error.message}`) })
}

function slettMappe(mappe) {

    mappeUrl = `/api/folders/${mappe}`

    fetch(`${mappeUrl}`, {
        method: 'DELETE'
    })
    .then(response => { if (! response.ok) alert("Feil 23: Kunne ikke slette mappe: " + mappe) } )
    .then(result => { window.location.reload() } )
    .catch(error => { alert(`Sletting feilet\n${error.message}`) })
}



function arkiverArtikkel(id) {

    urlArkivering = `/api/articles/${id}?cmd=archive`
    
    fetch(`${urlArkivering}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
    })
    .then(response => { if (! response.ok) alert("Feil 31: Kunne ikke arkivere artikkel.\nIdentifikator: " + id) } )
    .then(result => { window.location.reload() } )
    .catch(error => { alert(`Arkivering feilet\n${error.message}`) })

}

function slettArtikkel(id) {

    urlSletting = `/api/articles/${id}`
    
    fetch(`${urlSletting}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
    })
    .then(response => { if (! response.ok) alert("Feil 27: Kunne ikke slette artikkel.\nIdentifikator: " + id) } )
    .then(result => { window.location.reload() } )
    .catch(error => { alert(`Sletting feilet\n${error.message}`) })

}

function lagreArtikkel(mappe) {
    
    let url = document.getElementById("urlInput").value

    postArtikkel( create_json_message(url, mappe, url) );
}

function postArtikkel(artikkelData) {

    fetch('/api/articles', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(artikkelData)
    })
    .then(response => {
        if (response.ok) {
            window.location.reload()
        } else {
            alert(`Lagring feilet\n${response.status} - ${response.statusText}`)
        }
    })
    .catch(error => {
        alert(`Lagring feilet\n${error.message}`)
    })
}

function create_json_message(tittel, mappe, url) {
    return {
        tittel,
        mappe,
        url,
    }
}
