#!/bin/bash

if [ $# -ne 1 ]; then
  echo Ingen installasjonskatalog angitt. Avslutter installasjonen.
  exit 2
fi

SOURCE=`dirname "$0"`
TARGET=$1
CONF=$TARGET/lesedata/conf

if [ -d $CONF ]; then
  echo Katalogen eksisterer allerede, avslutter installasjonsprosessen.
  exit 2
fi

echo Oppretter $CONF
mkdir -p $CONF

echo Kopierer konfigurasjonsmaler til $CONF
cp $SOURCE/../conf/leseliste.conf.template $CONF/leseliste.conf
cp $SOURCE/../conf/log4j.properties.template $CONF/log4j.properties

echo Husk å redigere lesedata.conf


