package no.leseliste.conf

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import no.leseliste.api.artikkel.ArtikkelRessurs
import no.leseliste.api.mappe.MappeRessurs
import no.leseliste.api.tjenestestatus.TjenestestatusRessurs
import no.leseliste.app.artikkel.ArtikkelController
import no.leseliste.app.mappe.{MappeController, RootController}
import no.leseliste.tjeneste.artikkel.{ArtikkelRepository, ArtikkelTjeneste}
import no.leseliste.tjeneste.mappe.{MappeRepository, MappeTjeneste}
import org.slf4j.LoggerFactory

class LeselisteFactory(konf: Konfigurasjonsleser) {

  private val LOG = LoggerFactory.getLogger(this.getClass)

  private val jsonMapper = new ObjectMapper() with ScalaObjectMapper
  jsonMapper.registerModule(DefaultScalaModule)
  jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

  private val tjenesteStatusRessurs = new TjenestestatusRessurs
  //private val authRessurs = new AuthRessurs(new File("./conf/passwords"), jsonMapper)

  private val mappeRepo = new MappeRepository(konf.datakatalog)
  private val mappetjeneste = new MappeTjeneste(mappeRepo)
  private val mappeRessurs = new MappeRessurs(mappetjeneste, jsonMapper)

  val artikkelRepo = new ArtikkelRepository(konf.datakatalog)
  private val artikkelTjeneste = new ArtikkelTjeneste(artikkelRepo)
  private val artikkelRessurs = new ArtikkelRessurs(artikkelTjeneste, jsonMapper)

  private val artikkelController = new ArtikkelController(artikkelTjeneste)
  private val mappeController = new MappeController(mappetjeneste)
  private val rootController = new RootController(mappetjeneste)

  // hver enktelt ressurs burde kunne svare på dette for oppstart og for tjenestestatusformål
  private var erAutentiseringStartet = false   
  private var erTjenestestatusStartet = false
  
  
  def startTjenestestatus = {
    if (!erTjenestestatusStartet) {
      tjenesteStatusRessurs.start
      erTjenestestatusStartet = true
    }  
  }

  def startAutentisering = {
    if (!erAutentiseringStartet) {
      // authRessurs.start  TODO
      erAutentiseringStartet = true
    }  
  }
  
  def startAPI {
    if (konf.develMode) sjekkProduksjonsmiljoe
    
    mappeRessurs.start
    artikkelRessurs.start
  }

  def startWeb {
    if (!konf.develMode) sjekkProduksjonsmiljoe
    
    mappeController.start
    artikkelController.start
    rootController.start
  }

  def sjekkProduksjonsmiljoe {

    if (!erTjenestestatusStartet || !erAutentiseringStartet) {
      // TODO: skriv til stderr?
      LOG.warn("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
      LOG.warn("!!!                                                                  !!!")
      LOG.warn("!!! Tjenestene Autentisering eller Status er ikke startet skikkelig  !!!")
      LOG.warn("!!!                                                                  !!!")
      LOG.warn("!!!                                                                  !!!")
      LOG.warn("!!!                En feil har oppstått, SØK HJELP!!                 !!!")
      LOG.warn("!!!                                                                  !!!")
      LOG.warn("!!!                                                                  !!!")
      LOG.warn("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    }
  }
}
