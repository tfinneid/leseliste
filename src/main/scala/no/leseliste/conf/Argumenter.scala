package no.leseliste.conf

import java.io.File

object Argumenter {

  def parse(version: String, args: Array[String]) = {

    def sjekkEkstraArg(a: String, i: Int) = {
      if (args.length-1 < i) { ukjentArgument(s"""Argumentet "$a" mangler verdi.""" ); i}
      else i
    }

    var configFile = ""

    var i = 0;
    while (i < args.length) {
      args(i) match {
        case "-c" => { i=sjekkEkstraArg("-c",i+1); configFile = checkConfFile(args(i)) }
        case "-h" | "--help" => printHelp
        case "-v" | "--version" => printVersion(version)
        case u => ukjentArgument("Ukjent argument " + u)
      }
      i+=1
    }
    configFile
  }

  def ukjentArgument(msg: String)= {
    println(msg)
    System.exit(2)
  }

  def printHelp: Unit = {
    println( formatHelp )
    System.exit(0)
  }

  def printVersion(version: String): Unit = {
    val rev = Option(System.getProperty("prog.revision"))
    val revStr = if (rev.isEmpty) "" else s" (revision ${rev.get})"

    println(s"Leseliste v$version " + revStr);
    System.exit(0)
  }

  def checkConfFile(filename: String) = {
    val f = new File(filename)
    if (!f.exists) {
      System.err.println("Argument config file does not exists. Aborting.")
      System.exit(2)
    }
    f.getCanonicalPath
  }

  def formatHelp = {
    s"""Usage: program [options...]
       |   -c FILE      Configuration file
       |   -h           Print this help text and quit
       |   -v           Print version number and quit
       |
       |The default configuration file search order is 1) command line argument 2) current directory 3) home directory""".stripMargin
  }
}