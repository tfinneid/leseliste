package no.leseliste.conf

import java.io.{File, FileReader}
import java.util.Properties

import no.leseliste.felles.Hjelper._

class Konfigurasjonsleser(argConfigFil: String = "") {

  val progHome = {
    val progDir = System.getProperty("prog.home")
    if ( ikkeTom(progDir) ) progDir
    else "."
  }

  val MODUS = "mode"
  val HOST = "host"
  val PORT = "port"
  val LOGG_KONFIGURASJON = "loggkonfigurasjon"
  val LOGG_FIL = "loggfil"
  val DATA_KATALOG = "datakatalog"

  val APP_KONFIGURASJON = "appkonfigurasjon"
  // Ikke konnfigurerbar verdi
  val LOGG_APPENDER_NAVN = "log4j.appender.mainLog.File"

  private val standardstier = finnConfigSti

  private val standardverdier = new Properties()
  standardverdier.put(MODUS, "devel")
  standardverdier.put(HOST , "127.0.0.1")
  standardverdier.put(PORT , "6877")

  standardverdier.put(LOGG_KONFIGURASJON , s"${standardstier.confdir}/log4j.properties")
  standardverdier.put(LOGG_FIL , s"${standardstier.loggdir}/leseliste.log")
  standardverdier.put(DATA_KATALOG , standardstier.datadir)

  standardverdier.put(APP_KONFIGURASJON , s"${standardstier.confdir}/${standardstier.standardConfigFilnavn}")

  lesKonfigurasjon(argConfigFil)

  def develMode = {
    val develModeString = standardverdier.getProperty(MODUS)
    erTom(develModeString) || ! "production".equalsIgnoreCase(develModeString)
  }

  def port = standardverdier.getProperty(PORT).toInt

  def host = standardverdier.getProperty(HOST)

  def loggkonfigurasjon = standardverdier.getProperty(LOGG_KONFIGURASJON)

  def loggfil = standardverdier.getProperty(LOGG_FIL)

  def datakatalog = standardverdier.getProperty(DATA_KATALOG)

  private def konfigurasjon = standardverdier.getProperty(APP_KONFIGURASJON)

  private def lesKonfigurasjon(argKonfigFil: String) {
    val konfigFile = new File(
      if (ikkeTom(argKonfigFil)) argKonfigFil else  konfigurasjon
    )

    if (konfigFile.exists()) {
      val p = lesConfFile(konfigFile)
      settInn(p)
    } else {
      System.out.println("INFO: Bruker default konfigurasjon")
    }
  }

  private def settInn(props: Properties) {
    props.keySet().forEach( k => {
      val key = k.asInstanceOf[String]
      if (standardverdier.containsKey(k)) standardverdier.setProperty( key, props.getProperty(key) )
      else {
        System.err.println("Ukjent konfigurasjonsparameter: " + key)
        System.exit(2)
      }
    })
  }

  private def lesConfFile(sti: File): Properties = {
    println(s"Henter konfigurasjon fra ${sti.getCanonicalPath}")

    val props = new Properties()
    try {
      props.load(new FileReader(sti))
    } catch {
      case e: Throwable => {
        System.err.println("Feil ved lesing av konfigurasjonsfil")
        System.exit(2)
      }
    }
    props
  }

  private def finnConfigSti: Configstier = {
    val userHome = System.getProperty("user.home")
    val confsti = new Configstier()

    def configFilFinnes(sti: String) = {
      try {
        new File(s"$sti/${confsti.confdir}/${confsti.standardConfigFilnavn}" ).exists()
      } catch {
        case _: Exception => false
      }
    }

    if (new File(argConfigFil).exists())
      confsti
    else if ( configFilFinnes(".") )
      confsti
    else if ( configFilFinnes(userHome) )
      new Configstier(userHome)
    else {
      System.err.println("Finner ingen konfigurasjon, kan ikke starte.")
      System.exit(2)
      confsti // Nås aldri men kreves som returverdi
    }
  }
}

class Configstier(private val rotsti: String = ".") {

  val standardConfigFilnavn = "leseliste.conf"

  val lesedatadir = "lesedata"
  val datadir = s"$rotsti/$lesedatadir/data"
  val loggdir = s"$rotsti/$lesedatadir/logg"
  val confdir = s"$rotsti/$lesedatadir/conf"
}