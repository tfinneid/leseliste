package no.leseliste

import java.io.{File, FileInputStream}
import java.net.InetAddress
import java.util.function.Consumer

import no.leseliste.conf.{Argumenter, Konfigurasjonsleser, LeselisteFactory}
import org.slf4j.LoggerFactory
import spark.{Route, Spark}
import spark.Spark._

import scala.collection.JavaConverters.asScalaSet
import scala.io.Source
import no.leseliste.felles.Hjelper._

object Leseliste {

  def main(args: Array[String]): Unit = {
    try {
      val l = new Leseliste(args)
      l.start
    } catch {
      case e: Exception => unrecoverableError(e)
    }
  }

  def unrecoverableError(e: Exception) {
    System.err.println("FATAL: An irrecoverable error occured: " + e.getMessage)
    System.exit(1)
  }
}

class Leseliste(args: Array[String]) {

  val versjon = hentVersjon
  val argConfig = Argumenter.parse(versjon, args)
  val konf = new Konfigurasjonsleser(argConfig)

  konfigurerLogging
  private val LOG = LoggerFactory.getLogger(this.getClass)

  val factory = new LeselisteFactory(konf)

  def start: Unit = {

    LOG.info(s"Starter leseliste for v$versjon")

    Spark.initExceptionHandler(defaultExceptionHandler)

    sjekkTilstand
    konfigurerKjøremodus

    Spark.init

    factory.startTjenestestatus
    factory.startAutentisering

    factory.startAPI
    factory.startWeb

    System.out.println("Started Leseliste.")
  }

  def konfigurerKjøremodus: Unit = {

    if (konf.develMode) {
      LOG.info("Starter tjener i utviklermodus")
      develmodus
    } else {
      LOG.info("Starter tjener i produksjonsmodus")
      produksjonsmodus
    }

    afterAfter((req, response) => {
      response.header("Server", "Leseliste")
      LOG.info(s"${req.requestMethod()} ${req.url()} (${req.ip()}) - (HTTP response: ${response.status()}) ")
    })

    def develmodus: Unit = {

      val networkInterface = konf.host
      val portNum = konf.port

      if (! InetAddress.getByName(networkInterface).isLoopbackAddress) {
        LOG.warn("********                                                                                                                          ********")
        LOG.warn("********                        Tjener startet i utviklermodus, på eksternt nettverksgrensesnitt.                                 ********")
        LOG.warn("********                                                                                                                          ********")
        LOG.warn("********           Enkelte beskyttelsesmekanismer er ikke skrudd på og gjør tjeneren sårbar på et eksternt grensesnitt,           ********")
        LOG.warn("********           Hvis dette ikke er tiltenkt, anbefales det å endre konfigurasjonen til enten produksjonsmodus eller            ********")
        LOG.warn("********           endre til det lokale grensesnittet.                                                                            ********")
        LOG.warn("********                                                                                                                          ********")
      }

      if (portNum < 1000) {
        System.err.println("FATAL: Tjeneren må bruke en port som ikke krever administratorrettigheter. Konfigurer port verdien til en verdi mellom 1000 og 65535")
        System.exit(1)
      }

      ipAddress(konf.host)
      port(konf.port)

      staticFiles.externalLocation("src/main/public/")
      aktiverForespørselslogging
      aktiverCORSStøtte

      def aktiverCORSStøtte: Unit = {
        afterAfter((request, response) => {
          response.header("Access-Control-Allow-Origin", "*")
          response.header("Access-Control-Allow-Methods", "*")
          response.header("Access-Control-Allow-Headers", "*")
          response.header("Origin", "localhost")
        })
        options("/*", "*/*", optionsOk)
      }

      def optionsOk: Route = (req, res) => {
        res.status(200)
        "OK"
      }
    }

    def produksjonsmodus: Unit = {

      val networkInterface = konf.host
      val portNum = konf.port

      if (erTom(networkInterface) || InetAddress.getByName(networkInterface).isLoopbackAddress) {
        LOG.warn("********                                                                                                             ********")
        LOG.warn("********                 Tjeneren starter i produksnonsmodus på et lokalt nettverksgrensesnitt.                      ********")
        LOG.warn("********                Noen ressurser kan mangle hvis kjøremiljøet ikke er et produksjonsmiljø.                     ********")
        LOG.warn("********                                                                                                             ********")
      }

      ipAddress(networkInterface)
      port(portNum)

      staticFiles.externalLocation(s"${konf.progHome}/public/")
    }

    def aktiverForespørselslogging: Unit = {
      before((req, response) => {
        LOG.debug(s"> ${req.requestMethod()} ${req.url()} (${req.ip()}) ")
        asScalaSet(req.headers()).foreach( h =>
          LOG.debug(s"> Header: $h : ${req.headers(h)}")
        )
      })
    }
  }

  def konfigurerLogging = {
    import org.apache.log4j.PropertyConfigurator
    import java.io.FileInputStream
    import java.io.IOException
    import java.util.Properties

    val log4JPropertyFile = konf.loggkonfigurasjon

    val p = new Properties

    try {
      p.load(new FileInputStream(log4JPropertyFile))
      p.setProperty(konf.LOGG_APPENDER_NAVN, konf.loggfil)
      PropertyConfigurator.configure(p)
    } catch {
      case e: IOException => {
        System.err.println("FATAL: Could not configure logging. Aborting.\n" + e.getMessage)
        System.exit(1)
      }
    }
  }

  def hentVersjon = {
    val envVersjon = System.getProperty("prog.version")
    val versjonsFil = "version.txt"

    if ( ! erTom(envVersjon) )
      envVersjon
    else if ( new File(versjonsFil).exists() )
      lesVersjon(versjonsFil)
    else {
      System.err.println("Could not read version information: " + versjonsFil)
      "<Ukjent versjon>"
    }
  }

  def lesVersjon(versjonsFil: String) = {
    var v = "<Ukjent version>"

    try {
      val in = new FileInputStream( versjonsFil )
      val f = Source.fromInputStream(in)
      v = f.getLines.mkString
      f.close
    } catch {
      case e: Throwable => {
        System.err.println("<4>Could not read version information from file " + e.getMessage)
        LOG.warn("Could read version file. " + e)
      }
    }
    v
  }

  def sjekkTilstand {
    LOG.info("Sjekker tilstand...")
    // TODO sjekk at systemet fungerer (disk/katalog eksisterer), sjekk konsistens på data...

    factory.artikkelRepo.sjekkKonsistens
  }

  def defaultExceptionHandler: Consumer[Exception] = {
    (e: Exception) => {
      import Leseliste.unrecoverableError
      unrecoverableError(e)
    }
  }
}
