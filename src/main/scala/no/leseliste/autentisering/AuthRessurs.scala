package no.leseliste.autentisering

import java.io.{File, FileReader}
import java.util.{Date, Properties}

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.{JsonMappingException, ObjectMapper}
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import no.leseliste.api.RessursHjelper
import no.leseliste.api.RessursHjelper.{haltException, logRequest}
import no.leseliste.tjeneste.artikkel.TjenesteHjelper.{isEmpty, isNotEmpty}
import org.slf4j.LoggerFactory
import spark.{HaltException, Route}
import spark.Spark._

import scala.collection.mutable

class AuthRessurs(private val passwordsFile: File, jsonMapper: ObjectMapper with ScalaObjectMapper) {

  private val LOG = LoggerFactory.getLogger(this.getClass)

  private val passwordFile = new FileReader(passwordsFile)
  private val passwords = new Properties()
  private val tokens = mutable.Map[String, java.util.Date]()

  def start = {
    passwords.load(passwordFile)
    passwords.forEach( (u,p) => println("[DEBUG] Users:  " + u + ":" + p))

    post("/auth/authenticate", "application/json",  authenticateUser)
    post("/auth/authorize", "application/json",  authorizeUser)
  }

  def authenticateUser: Route = (req, res) => {
    logRequest(req)

    var loginUsername = "(Unknown)"
    // TODO eventuell aktive men nå gamle tokens for brukeren må ugyldiggjøres
    try {
      val authRequest: AuthenticateRequest = jsonMapper.readValue[AuthenticateRequest](req.body)
      loginUsername = authRequest.username

      if (isEmpty(authRequest.username) ||
        isEmpty(authRequest.password)) {
        LOG.info(s"User authentication failure for UNKNOWN user: ${loginUsername}")
        RessursHjelper.haltException(401, "Unauthorized")
      }

      val requestUsername = authRequest.username.trim
      val requestPassword = authRequest.password.trim
      LOG.info(s"Authenticating attempt for user: $requestUsername")

      if (isNotEmpty(requestUsername) &&
          isNotEmpty(requestPassword) &&
          passwords.containsKey(requestUsername) &&
          requestPassword == passwords.getProperty(requestUsername)
      ) {
        val token = new Date().getTime.toString
        tokens(token) = new Date()

        val authResponse = AuthenticationToken(token)
        LOG.info(s"Authenticating succeeded for user: $requestUsername")
        res.`type`("application/json")
        res.header("Cache-Control", "no-cache")
        res.status(200)
        jsonMapper.writeValueAsString(authResponse) + "\n\n"
      } else {
        LOG.info(s"Authentication failure for $loginUsername")
        RessursHjelper.haltException(401, "Unauthorized")
      }
    } catch {
      case e: HaltException => throw e
      case e @ (_: JsonParseException | _: JsonMappingException) => haltException(400, "Feil på melding for autentisering.")
      case e: Throwable => haltException(500, "Server Error")
    }
  }

  def authorizeUser: Route = (req, res) => {
    logRequest(req)

    try {
      val authorizeRequest: AuthenticationToken = jsonMapper.readValue[AuthenticationToken](req.body)

      if (isEmpty(authorizeRequest.token)) {
        LOG.info(s"Token authorization failure")
        RessursHjelper.haltException(401, "Unauthorized")
      }

      val requestToken = authorizeRequest.token.trim

      val aDayAgo = new Date().getTime - 100000000   // TODO må være 24 timer?

      if (isNotEmpty(requestToken) &&
        tokens.contains(requestToken) &&
        tokens(requestToken).getTime > aDayAgo
      ) {
        tokens(requestToken) = new Date()  // Oppdaterer tidstempel

        res.header("Auth-Token", s"$requestToken")
        res.header("Cache-Control", "no-cache")
        res.status(204)
        ""
      } else {
        RessursHjelper.haltException(401, "Unauthorized")
      }
    } catch {
      case e: HaltException => throw e
      case e @ (_: JsonParseException | _: JsonMappingException) => haltException(400, "Feil ved autentiseringsbeviset")
      case e: Throwable => haltException(500, "Server Error")
    }
  }
}

case class AuthenticateRequest(username: String, password: String)

case class AuthenticationToken(token: String)
