package no.leseliste.api.artikkel

import java.io.FileNotFoundException

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.{JsonMappingException, ObjectMapper}
import com.fasterxml.jackson.databind.exc.InvalidDefinitionException
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import no.leseliste.api.exceptions.{ClientFaultException, NotFoundException}
import no.leseliste.tjeneste.artikkel.{Artikkel, ArtikkelTjeneste}
import org.slf4j.LoggerFactory
import spark.Route
import spark.Spark._

class ArtikkelRessurs(tjeneste: ArtikkelTjeneste, jsonMapper: ObjectMapper with ScalaObjectMapper) {

  require(tjeneste != null)
  require(jsonMapper != null)
  
  import no.leseliste.api.RessursHjelper._

  private val LOG = LoggerFactory.getLogger(this.getClass)

  def start: Unit = {
        get("/api/articles", "application/json", getArticlesList)  // parameters: ?folder=FOLDERNAME  ?archived=true
        get("/api/articles/:articleId", "application/json", getArticle)
        post("/api/articles", "application/json", addArticle)
        delete("/api/articles/:articleId", "application/json", deleteArticle)
        put("/api/articles/:articleId", "application/json", updateArticle) // parameters: ?cmd=archive|move ?newFolder=FOLDERNAME
  }

  def getArticlesList: Route = (req, res) => {
    logRequest(req)
    LOG.info(s"Retrieving json list of articles ")
    
    val artikkelListe = try {
      val mappeNavn = Option(req.queryParams("folder"))getOrElse( tjeneste.kronologiMappe )
      val arkivert = Option(req.queryParams("archived")).getOrElse("false").toBoolean

      tjeneste.hentArtikkelListe(mappeNavn, arkivert )
    } catch {
      case e @ (_: ClientFaultException | _: IllegalArgumentException) => haltException(400, e.getMessage)
//      case e: ClientFaultException => haltException(400, e.getMessage)
      case e: Throwable => haltException(500, e.getMessage)
    }

    res.`type`("application/json")
    res.status(200)
    jsonMapper.writeValueAsString(artikkelListe)
  }

  def addArticle: Route = (req, res) => {
    logRequest(req)

    val artikkelId = try {
      val artikkel: Artikkel = jsonMapper.readValue[Artikkel](req.body)
      LOG.info(s"Adding article $artikkel")
      tjeneste.lagreArtikkel(artikkel)
    } catch {
      case e @ (_: JsonParseException | _: JsonMappingException) => haltException(400, "Feil på melding for å opprette artikkel.")
      case e: ClientFaultException => haltException(400, e.getMessage)
      case e: Throwable => haltException(500, e.getMessage)
    }

    val location = s"${req.scheme()}://${req.host()}${req.pathInfo}/${artikkelId}"
    res.status(201)
    res.header("Location", s"${location}")
    ""
  }

  def getArticle: Route = (req, res) => {
    logRequest(req)
    val artikkelId = req.params(":articleId")
    LOG.info(s"Retrieving article $artikkelId")

    val artikkel = try {
      tjeneste.hentArtikkel(artikkelId)
    } catch {
      case e: NotFoundException => haltException(404, e.getMessage)
      case e: ClientFaultException => haltException(400, e.getMessage)
      case e: Throwable => haltException(500, e.getMessage)
    }

    res.`type`("application/json")
    res.status(200)
    jsonMapper.writeValueAsString(artikkel)
  }

  def deleteArticle: Route = (req, res) => {
    logRequest(req)
    val artikkelId = req.params(":articleId")
    LOG.info(s"Deleting article #$artikkelId")

    try {
      tjeneste.slettArtikkel(artikkelId)
    } catch {
      case e: NotFoundException => haltException(404, e.getMessage)
      case e: ClientFaultException => haltException(400, e.getMessage)
      case e: Throwable => haltException(500, e.getMessage)
    }
    res.status(204).toString
  }

  def updateArticle: Route = (req, res) => {
    logRequest(req)

    val artikkelId = req.params(":articleId")
    val cmd = Option(req.queryParams("cmd"))

    res.`type`("application/json")
    cmd match {
      case Some("archive") => {
        LOG.info(s"Archiving article #${artikkelId}")
        tjeneste.arkiverArtikkel(artikkelId)
      }
      case Some("move") => {
        LOG.info(s"Moving article #${artikkelId}")
        val folder = Option(req.queryParams("newFolder"))
        tjeneste.flyttArtikkel(artikkelId, folder)
      }
      case _ => {
        LOG.info(s"Unknown command for article #${artikkelId} with command ${cmd.getOrElse("ukjent")}")
        haltException(400, "Unknown command")
      }
    }

//    val location = s"${req.scheme()}://${req.host()}${req.pathInfo}/${artikkelId}"
    res.status(201)
//    res.header("Location", s"${location}")
    ""
  }
}
