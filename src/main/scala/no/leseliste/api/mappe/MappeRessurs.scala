package no.leseliste.api.mappe

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import no.leseliste.api.exceptions.{AlreadyExistsException, ClientFaultException, NotEmptyException}
import no.leseliste.tjeneste.mappe.MappeTjeneste
import org.slf4j.LoggerFactory
import spark.Route
import spark.Spark._

/**
  * Tilbyr REST tjenestegrensesnittet etter HTTP standarden
  *
  * - parser og validerer requester kun etter nettverks og HTTP regler
  * - konvertere responser til korrekt respons type
  * - konvertere exceptions til HTTP status koder.
  */
class MappeRessurs(mappeTjeneste: MappeTjeneste, jsonMapper: ObjectMapper with ScalaObjectMapper) {

  require(mappeTjeneste != null)
  require(jsonMapper != null)

  import no.leseliste.api.RessursHjelper._

  private val LOG = LoggerFactory.getLogger(this.getClass)

  def start = {

    get("/api/folders", "application/json",  getFolderList)
    post("/api/folders/:folderName", "application/json",  addFolder)
    post("/api/folders/:folderName/empty", "application/json",  emptyFolder)  // TODO PUT i stedet? er endring, ikke nyopprettelse
    delete("/api/folders/:folderName",  "application/json", deleteFolder)
    put("/api/folders/:folderName",  "application/json", changeFolderName) // parameters: ?newname=FOLDERNAME
  }

  def getFolderList: Route = (req, res) => {
    logRequest(req)

    LOG.info(s"Retrieving json list of folders")

    val mappeListe = try {
      mappeTjeneste.hentMappeListe
    } catch {
      case e: Throwable => haltException(500, e.getMessage)
    }

    res.`type`("application/json")
    res.status(200)
    jsonMapper.writeValueAsString(mappeListe) + "\n\n"
  }

  def addFolder: Route = (req, res) => {
    logRequest(req)

    val mappenavn = req.params(":folderName")

    if (req.body().length > 0) haltException(418, "No request body allowed.")

    LOG.info(s"Adding folder $mappenavn")
    val artikkelId = try {
      mappeTjeneste.opprettMappe(mappenavn)
    } catch {
      case e: AlreadyExistsException => haltException(409, e.getMessage)
      case e: ClientFaultException => haltException(400, e.getMessage)
      case e: Throwable => haltException(500, e.getMessage)
    }

    val location = s"${req.scheme()}://${req.host()}${req.pathInfo}"
    res.status(201)
    res.header("Location", s"${location}")
    ""
  }

  def deleteFolder: Route = (req, res) => {
    logRequest(req)

    val mappenavn = req.params(":folderName")
    LOG.info(s"Deleting folder $mappenavn")
    try {
      mappeTjeneste.slettMappe(mappenavn)
    } catch {
      case e: NotEmptyException => haltException(409, e.getMessage)
      case e: ClientFaultException => haltException(400, e.getMessage)
      case e: Throwable => haltException(500, e.getMessage)
    }

    res.status(204)
    ""
  }

  def emptyFolder: Route = (req, res) => {
    logRequest(req)

    val mappenavn = req.params(":folderName")
    LOG.info(s"Emptying folder $mappenavn")
    try {
      mappeTjeneste.tømMappe(mappenavn)
    } catch {
      case e: ClientFaultException => haltException(400, e.getMessage)
      case e: Throwable => haltException(500, e.getMessage)
    }

    res.status(204)
    ""
  }

  def changeFolderName: Route = (req, res) => {
    logRequest(req)

    val mappenavn = req.params(":folderName")
    val nyttnavn = Option(req.queryParams("newname"))

    if (nyttnavn.isEmpty) {
      res.status(400)
      """{ "message" : "missing parameter: newname" }"""
    } else {

      LOG.info(s"Changing name of folder $mappenavn to $nyttnavn")
      try {
        mappeTjeneste.endreMappe(mappenavn, nyttnavn.get)
      } catch {
        case e: ClientFaultException => haltException(400, e.getMessage)
        case e: Throwable => haltException(500, e.getMessage)
      }

      val location = s"${req.scheme()}://${req.host()}${req.uri}"
      res.status(201)
      res.header("Location", s"${location}")
      ""
    }
  }

//  private def changeUrl(req: Request) = {
//
//    val idx = req.uri.lastIndexOf("/")
//    req.uri.
//
//  }
}

