package no.leseliste.api.exceptions

class ClientFaultException(val message: String) extends Exception(message)

class ServerFaultException(val message: String)  extends Exception(message)

class NotEmptyException(override val message: String)  extends ClientFaultException(message)

class AlreadyExistsException(override val message: String)  extends ClientFaultException(message)

class NotFoundException(override val message: String)  extends ClientFaultException(message)