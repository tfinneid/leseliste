package no.leseliste.api.helse

import com.codahale.metrics.health.HealthCheck

class ApplikasjonsHelse extends HealthCheck {

//  def check = HealthCheck.Result.unhealthy("The great gig in the sky.....")
  def check = HealthCheck.Result.healthy("Learning to fly...")
}
