package no.leseliste.api

import spark.{HaltException, Request}
import spark.Spark.{halt, notFound}

import scala.collection.JavaConverters.asScalaSet
import org.slf4j.LoggerFactory

object RessursHjelper {

  private val LOG = LoggerFactory.getLogger(this.getClass)

//  private def logAccess(req: Request): Unit = {
//    LOG.debug(s"${req.requestMethod()} ${req.url()} (${req.ip()}) ")
//  }

  def logRequest(req: Request): Unit = {
//    logAccess(req)
//
//    asScalaSet(req.headers()).foreach( h =>
//      LOG.debug(s"Header: $h : ${req.headers(h)}")
//    )
//    if ( !req.body.isEmpty )
//      LOG.debug(s"Request Body:\n${req.body()}\n")
  }

  def haltException(code: Int, message: String): HaltException = {
    halt(code,  s"""{ "code" : "$code", "message" : "$message" }""")
  }
}
