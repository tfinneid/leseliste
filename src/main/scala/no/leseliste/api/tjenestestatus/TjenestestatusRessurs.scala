package no.leseliste.api.tjenestestatus

import java.util.Date

import com.codahale.metrics.health.{HealthCheck, HealthCheckRegistry}
import no.leseliste.api.helse.ApplikasjonsHelse
import org.slf4j.LoggerFactory
import spark.Route
import spark.Spark._

class TjenestestatusRessurs {
  import no.leseliste.api.RessursHjelper._

  private val LOG = LoggerFactory.getLogger(this.getClass)

  private val helseregister = new HealthCheckRegistry()

  def start {
    helseregister.register("applikasjon", new ApplikasjonsHelse())

    get("/tjenestestatus/ping", "application/json", pingRoute)
    get("/tjenestestatus/helse", "application/json", helsesjekk)
  }

  def pingRoute: Route = (req, res) => {
    logRequest(req)
    s"I'm awake!? Promise! (${new Date()})\n"
  }

  def helsesjekk: Route = (req, res) => {
    import collection.JavaConverters._

    helseregister.runHealthChecks().asScala.toMap
      .map{ case (k,v) => s"$k: ${v}" }
      .mkString("\n") + "\n"
  }
}
