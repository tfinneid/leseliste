package no.leseliste.felles

object Hjelper {

  def erTom(input: String): Boolean =
    input == null || input.trim.isEmpty

  def ikkeTom(input: String) = !erTom(input)
}
