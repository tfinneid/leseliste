package no.leseliste.felles

object WhitelistedChars {
  val lovligeTegn = """([\x20-\x7E\xA1-\xFF&&[^\x22\x27\x7E\x5C\x7C\xB4\x60\x3B\x24\x2E\x2F\x5B\x5D\x7B\x7D\xAD]]*)""".r

  def lovlig_ISO8859_1_Streng(str: String) = str match {
    case lovligeTegn(s) => true
    case _ => false
  }
}
