package no.leseliste.app.artikkel

import no.leseliste.api.RessursHjelper.{LOG, haltException, logRequest}
import no.leseliste.api.exceptions.ClientFaultException
import no.leseliste.tjeneste.artikkel.ArtikkelTjeneste
import org.slf4j.LoggerFactory
import spark.Route
import spark.Spark.get

class ArtikkelController(tjeneste: ArtikkelTjeneste) {

  private val LOG = LoggerFactory.getLogger(this.getClass)

  def start = {
    get("/articles.html", "text/html", getArticles)  // parameters: ?folder=FOLDERNAME
  }

  def getArticles: Route = (req, res) => {

    logRequest(req)
    LOG.info(s"Retrieving HTML list of articles ")

    val mappeNavn = Option(req.queryParams("folder"))getOrElse( tjeneste.kronologiMappe )
    val arkivert = Option(req.queryParams("archived")).getOrElse("false").toBoolean

    val artikkelListe = tjeneste.hentArtikkelListe(mappeNavn, arkivert)

    res.`type`("text/html")
    res.status(200)

    val tittel = if (arkivert) "Arkivert lesestoff for " else "Leseliste for "
    val mappeLenke = if (!arkivert) """<a href="/">Tilbake</a>""" else s"""<a href="${req.pathInfo()}?folder=$mappeNavn">Tilbake</a>""" 
    val arkivLenke = if (!arkivert) s"""<a href="${req.pathInfo()}?folder=$mappeNavn&archived=true">Arkiv</a>""" else """"""
    
    val tabell = for ( a <- artikkelListe ) yield {
      s"""<li><span class="listText"><a href="${a.url}" target="_blank" rel="noopener noreferrer">${a.tittel}</a></span>
         | ${ if (!arkivert) s"""<input type="button" onClick="slettArtikkel('${a.id.trim}')" value="Slett"> """ else """""" }
         | ${ if (!arkivert) s"""<input type="button" onClick="arkiverArtikkel('${a.id.trim}')" value="Arkiver">""" else """""" }
         |</li>""".stripMargin
    }

    s"""
        <!DOCTYPE html>
        <html>
          <head>
            <meta charset="utf-8">
            <script src="/static/leseliste.js"></script>
            <link href="/static/leseliste.css" rel="stylesheet">
            <title>Leseliste</title>
            <meta name="viewport" content="width=device-width, initial-scale=1">
          </head>
        <body>

          <div id="topp">
            $mappeLenke
            <h2>$tittel ${mappeNavn.capitalize}</h2>
            $arkivLenke
          </div>
  
          <div id="nyArtikkel">
            <input id="urlInput" type="text" class="nyEntitetInput" placeholder="https://www.aftenpost.no/artikkel.html" >
            <input type="button" onClick="lagreArtikkel('${mappeNavn}')" value="Lagre">
          </div>
          
          <div id=liste">
            ${
              if (tabell.length > 0)
                "<ul>\n" + tabell.mkString("\n") + "</ul>\n"
              else
                "<ul><li>Her er det plass til mye spennende lesestoff...</li></ul>"
            }
          </div>

        </body>
        </html>
     """
  }
  
  def getIcoImg(url: String) = {
    val aURL = new java.net.URL(url)
    val icoUrl = aURL.getProtocol +"://"+ aURL.getHost + "/favicon.ico"

    s"""<img class="ico" src="${icoUrl}">"""
  }
}
