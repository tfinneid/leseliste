package no.leseliste.app.mappe

import no.leseliste.tjeneste.mappe.MappeTjeneste
import org.slf4j.LoggerFactory
import spark.Route
import spark.Spark.{get, post}

class MappeController(tjeneste: MappeTjeneste) {

  private val LOG = LoggerFactory.getLogger(this.getClass)

  def start = {

    get("/folders.html",  "text/html", getFolderList)
  }

  def getFolderList: Route = (req, res) => {
    LOG.info(s"Retrieving html list of folders")

    val mapper = tjeneste.hentMappeListe

    res.`type`("text/html")
    res.status(200)
    KonverteringsHjelper.toHtml(mapper)
  }
}

object KonverteringsHjelper {

  def toHtml(liste: List[String]): String = {

    s"""
        <!DOCTYPE html>
        <html>
          <head>
            <meta charset="utf-8">
            <script src="/static/leseliste.js"></script>
            <link href="/static/leseliste.css" rel="stylesheet">
            <title>Leseliste</title>
            <meta name="viewport" content="width=device-width, initial-scale=1">
          </head>
        <body>

          <div id="topp">
            <h2>Leselister</h2>
          </div>

          <div id="nyArtikkel">
            <input id="mappeInput" type="text" class="nyEntitetInput" placeholder="Legg til ny leseliste...">
            <input type="button" onClick="opprettMappe()" value="Opprett">
          </div>

          <div id=liste">
            <ul>
              ${liste.map( v =>
                s"""<li><span class="listText"><a href="/articles.html?folder=$v">${v.capitalize}</a></span>
                   |<input type="button" onClick="slettMappe('${v.trim}')" value="Slett"></li>""".stripMargin
              ).mkString("\n")}
            </ul>
          </div>

        </body>
        </html>
     """
  }
}
