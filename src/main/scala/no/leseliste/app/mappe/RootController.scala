package no.leseliste.app.mappe

import no.leseliste.tjeneste.mappe.MappeTjeneste
import org.slf4j.LoggerFactory
import spark.Route
import spark.Spark.get

class RootController(tjeneste: MappeTjeneste) {

  private val LOG = LoggerFactory.getLogger(this.getClass)

  def start = {

    get("/",  "text/html", getFolderList)
  }

  def getFolderList: Route = (req, res) => {
    LOG.info(s"Retrieving html list of folders")

    val mapper = tjeneste.hentMappeListe

    res.`type`("text/html")
    res.status(200)

    KonverteringsHjelper.toHtml(mapper)
  }
}
