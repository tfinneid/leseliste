package no.leseliste.tjeneste.mappe

import java.io.{File, FileWriter}

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper

object MappeRepositoryHjelper {


  private val jsonMapper = new ObjectMapper() with ScalaObjectMapper
  jsonMapper.registerModule(DefaultScalaModule)
  jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

  def writeJsonToFile(filnavn: String, data: Iterable[String]): Unit = {
    val json = jsonMapper.writeValueAsString(data)

    val writer = new FileWriter(filnavn)
    writer.write(json)
    writer.close()
  }

  def setFilepermissionsToOwner(file: File, execute: Boolean): Unit = {
    if (file.isDirectory) {
      /** Nullstill filrettighetene */
      file.setReadable(false, false)
      file.setWritable(false, false)
      file.setExecutable(false, false)

      /** Kun eier skal ha tilgang */
      file.setReadable(true, true)
      file.setWritable(true, true)
      file.setExecutable(execute, true)
    }
  }
}
