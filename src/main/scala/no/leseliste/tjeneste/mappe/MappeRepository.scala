package no.leseliste.tjeneste.mappe

import java.io.{File, IOException}

import no.leseliste.api.exceptions.{AlreadyExistsException, NotEmptyException}
import no.leseliste.felles.WhitelistedChars.lovlig_ISO8859_1_Streng
import no.leseliste.tjeneste.artikkel.TjenesteHjelper._
import no.leseliste.tjeneste.mappe.MappeRepositoryHjelper._
import org.slf4j.LoggerFactory

/**
  * Exceptions:
  *  - IllegalArgumentException: ClientFault - bruker feil med mappeargument
  *  - IOException: ServerFault - feil ved IO operasjon mot mappe
  */

class MappeRepository(val sti: String = "./lesedata/data") {

  private val LOG = LoggerFactory.getLogger(this.getClass)

  def hentMappeListe = {
    new File(sti)
      .listFiles
      .filter(_.isDirectory)
      .map(_.getName)
  }

  def opprettMappe(mappenavn: String) = {

    LOG.debug(s"Oppretter katalog ${mappenavn}")

    if (isEmpty(mappenavn)) throw new IllegalArgumentException("Kan ikke opprette mappe uten navn.")
    if (!lovlig_ISO8859_1_Streng(mappenavn)) throw new IllegalArgumentException("Mappenavnet inneholder ulovlige tegn.")

    val mappe = new File(s"$sti/$mappenavn")

    if (mappe.exists)
      throw new AlreadyExistsException(s"Kan ikke opprette en allerede eksisterende mappe: $mappenavn")

    if (mappe.mkdir) {
      setFilepermissionsToOwner(mappe, true)
      true
    } else {
      throw new IOException(s"Kunne ikke opprette mappe av ukjente årsaker")
    }
  }

  def slettMappe(mappenavn: String) = {

    if (isEmpty(mappenavn)) throw new IllegalArgumentException("Kan ikke slette en mappe uten et navn.")
    if (!lovlig_ISO8859_1_Streng(mappenavn)) throw new IllegalArgumentException("Mappenavnet inneholder ulovlige tegn.")

    val mappe = new File(s"$sti/$mappenavn")

    if (!mappe.exists || !mappe.isDirectory)
      throw new IllegalArgumentException(s"Kan ikke slette en mappe som ikke eksisterer: $mappenavn")
    if (mappe.listFiles().length > 0) throw new NotEmptyException("Mappen er ikke tom.")

    if (mappe.delete) true else throw new IOException(s"Kunne ikke slette mappe av ukjente årsaker")
  }

  def tømMappe(mappenavn: String) = {

    if (isEmpty(mappenavn)) throw new IllegalArgumentException("Kan ikke tømme en mappe uten et navn.")
    if (!lovlig_ISO8859_1_Streng(mappenavn)) throw new IllegalArgumentException("Mappenavnet inneholder ulovlige tegn.")

    val mappe = new File(s"$sti/$mappenavn")

    if (!mappe.exists || !mappe.isDirectory)
      throw new IllegalArgumentException(s"Kan ikke tømme en mappe som ikke finnes: $mappenavn")

    if (mappe.listFiles.forall(_.delete)) true else throw new IOException(s"Kunne ikke tømme mappe av ukjente årsaker")
  }

  def endreMappenavn(navn: String, nyttNavn: String) = {

    if (isEmpty(navn)) throw new IllegalArgumentException("Kan ikke endre navn på en mappe uten et navn.")
    if (isEmpty(nyttNavn)) throw new IllegalArgumentException("Kan ikke gi mappe nytt navn, uten at navnet er angitt.")
    if (!lovlig_ISO8859_1_Streng(navn)) throw new IllegalArgumentException("Mappenavnet inneholder ulovlige tegn.")
    if (!lovlig_ISO8859_1_Streng(nyttNavn)) throw new IllegalArgumentException("Nytt mappenavnet inneholder ulovlige tegn.")

    val eksisterendeMappe = new File(s"$sti/$navn")
    val nyMappe = new File(s"$sti/$nyttNavn")

    if (!eksisterendeMappe.exists || !eksisterendeMappe.isDirectory)
      throw new IllegalArgumentException(s"Kan ikke endre navn på en mappe som ikke finnes: $navn")
    if (nyMappe.exists)
      throw new IllegalArgumentException(s"Kan ikke gi mappen et navn som allerede eksisterer: $nyttNavn")
    if (eksisterendeMappe.getName == nyMappe.getName)
      throw new IllegalArgumentException(s"Nytt og gammel navn er det samme: $nyttNavn")

    if (eksisterendeMappe.renameTo(nyMappe)) true else throw new IOException(s"Kunne ikke endre mappenavn av ukjente årsaker")
  }

}
