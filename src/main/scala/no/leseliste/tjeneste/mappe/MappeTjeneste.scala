package no.leseliste.tjeneste.mappe

import no.leseliste.api.exceptions.{AlreadyExistsException, ClientFaultException, NotEmptyException, ServerFaultException}
import org.slf4j.LoggerFactory

class MappeTjeneste(repository: MappeRepository) {

  require(repository != null)

  private val LOG = LoggerFactory.getLogger(this.getClass)

  def hentMappeListe: List[String] = {
    LOG.debug(s"Henter mappeliste")
    repository.hentMappeListe.toList
  }

  def opprettMappe(navn: String): Boolean = {
    LOG.debug(s"Oppretter mappe ${navn}")
    try {
      repository.opprettMappe(navn)
    } catch {
      case e: AlreadyExistsException => throw e
      case e: IllegalArgumentException => throw new ClientFaultException(e.getMessage)
      case e: Throwable => throw new ServerFaultException(e.getMessage)
    }
  }

  def slettMappe(navn: String): Boolean = {
    LOG.debug(s"Sletter mappe ${navn}")
    try {
      repository.slettMappe(navn)
    } catch {
      case e: NotEmptyException => throw e
      case e: IllegalArgumentException => throw new ClientFaultException(e.getMessage)
      case e: Throwable => throw new ServerFaultException(e.getMessage)
    }
  }

  def tømMappe(navn: String): Boolean = {
    LOG.debug(s"Sletter innholdet i mappen ${navn}")
    try {
      repository.tømMappe(navn)
    } catch {
      case e: IllegalArgumentException => throw new ClientFaultException(e.getMessage)
      case e: Throwable => throw new ServerFaultException(e.getMessage)
    }
  }

  def endreMappe(navn: String, nyttnavn: String): Boolean = {
    LOG.debug(s"Endrer navn på mappen ${navn}")
    try {
      repository.endreMappenavn(navn, nyttnavn)
    } catch {
      case e: IllegalArgumentException => throw new ClientFaultException(e.getMessage)
      case e: Throwable => throw new ServerFaultException(e.getMessage)
    }
  }
}
