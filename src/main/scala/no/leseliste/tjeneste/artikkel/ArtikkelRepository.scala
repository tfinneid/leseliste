package no.leseliste.tjeneste.artikkel

import java.io.{File, FileNotFoundException, FileWriter}
import java.nio.file.Files
import java.nio.file.attribute.BasicFileAttributes

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import no.leseliste.tjeneste.mappe.MappeRepositoryHjelper.setFilepermissionsToOwner
import org.slf4j.LoggerFactory

class ArtikkelRepository(val sti: String = "./lesedata/data") {

  private val LOG = LoggerFactory.getLogger(this.getClass)

  val jsonMapper = new ObjectMapper() with ScalaObjectMapper
  jsonMapper.registerModule(DefaultScalaModule)
  jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

  def lagreArtikkel(artikkel: Artikkel): String = {
    val mappeSti = new File(s"$sti/${artikkel.mappe}")
    if (!mappeSti.exists || mappeSti.isFile)
      throw new IllegalArgumentException(s"Mappen '${artikkel.mappe}' eksisterer ikke.")

    val artikkelFile = new File(mappeSti, artikkel.id)
    artikkelFile.createNewFile

    val json = jsonMapper.writeValueAsString(artikkel)

    val writer = new FileWriter(artikkelFile)
    writer.write(json)
    writer.close()
    artikkel.id
  }

  def slettArtikkel(artikkelId: String) =
    finnArtikkelSti(artikkelId) match {
      case Some(f: File) => f.delete
      case None => throw new FileNotFoundException("Artikkelen finnes ikke.")
  }

  def hentArtikkel(artikkelId: String): Artikkel =
    finnArtikkelSti(artikkelId) match {
      case Some(f: File) => jsonMapper.readValue[Artikkel](f)
      case None => throw new FileNotFoundException("Artikkelen finnes ikke.")
  }

  def hentArtikler(mappe: String, arkiv: Boolean): List[Artikkel] = {
    LOG.debug(s"Henter artikkelliste fra mappelager '$mappe'")

    val mappeSti = new File(s"$sti/${mappe}")
    if (!mappeSti.exists || mappeSti.isFile) {
      LOG.warn(s"Ingen mappe med navn $mappe")
      throw new IllegalArgumentException(s"Mappen '${mappe}' finnes ikke.")
    }

    val arkivert = (f: File) => f.getName.endsWith(".archived")
    val lastAccess: (File) => Long = (f: File) => 
      Files.readAttributes(f.toPath, classOf[BasicFileAttributes]).lastAccessTime.toMillis
    
    if (arkiv)      
      mappeSti.listFiles()
        .filter( arkivert )
        .sortWith( lastAccess(_) > lastAccess(_) )
        .map( jsonMapper.readValue[Artikkel](_) )
        .toList
    else
      mappeSti.listFiles()
        .filter( !arkivert(_) )
        .map( jsonMapper.readValue[Artikkel](_) )
        .toList
        .sortWith(_.tidsstempel > _.tidsstempel)
  }

  def flyttArtikkel(artikkelId: String, mappe: String) = {
    finnArtikkelSti(artikkelId) match {
      case Some(f: File) => {
        val mappeSti = new File(s"$sti/${mappe}/${artikkelId}")
        LOG.debug("Flytter artikkel fra " + f.getAbsolutePath + "til" + mappeSti.getAbsolutePath )
        f.renameTo(mappeSti)
      }
      case None => throw new FileNotFoundException("Artikkelen finnes ikke.")
    }
  }

  def arkiverArtikkel(artikkelId: String) = {
    finnArtikkelSti(artikkelId) match {
      case Some(f: File) => {
        val mappeSti = new File(f.getParent, artikkelId + ".archived")
        LOG.debug(s"Arkiverer artikkel i ${mappeSti}" )
        f.renameTo(mappeSti)
      }
      case None => throw new FileNotFoundException("Artikkelen finnes ikke.")
    }
  }

  def finnArtikkelSti(artikkelId: String): Option[File] = {
    val filPath = new File(sti).toPath
    val fil = Files.walk(filPath).filter {
      f => f.toFile.getName == artikkelId
    }.findFirst

    if (fil.isPresent && fil.get.toFile.isFile)
      Some(fil.get.toFile)
    else None
  }

  def sjekkKonsistens: Unit = {

    val dataRot = new File(sti).getParent

    sjekkKatalog(dataRot)
    sjekkKatalog(sti)
    sjekkKatalog(sti + "/chrono")
    sjekkKatalog(sti + "/../logg")
    
    LOG.info("Tilstand OK.")
  }

  private def sjekkKatalog(katalog: String) = {

    val katalogSti = new File(katalog)
    if (!katalogSti.exists) {
      LOG.info("Oppretter mangelnde standardkatalog: " + katalogSti.getCanonicalPath)
      katalogSti.mkdir
    }

    setFilepermissionsToOwner(katalogSti, true)
  }
}
