package no.leseliste.tjeneste.artikkel

import java.io.{File, FileNotFoundException, InputStreamReader}
import java.net.{HttpURLConnection, URL, URLConnection}
import java.util.Date
import java.security.SecureRandom

import no.leseliste.api.exceptions.{ClientFaultException, NotFoundException, ServerFaultException}
import no.leseliste.tjeneste.artikkel.Artikkel.randGen
import no.leseliste.tjeneste.artikkel.TjenesteHjelper._
import no.leseliste.tjeneste.artikkel.UrlLeser.sjekkForUfullstendigTittel
import org.slf4j.LoggerFactory

import scala.io.Source
import scala.util.Random

object Artikkel {
  val randGen = new Random(new SecureRandom())
}

case class Artikkel(tittel: String, mappe: String, url: String) {

  val tidsstempel: Long = new Date().getTime
  val id: String = tidsstempel + "," + randGen.alphanumeric.take(45).mkString  // 62^45 tegn == 10^80 == >2^256

  require(isNotEmpty(id))
  require(tidsstempel > new Date().getTime - 3600)

  require(isNotEmpty(tittel))
  require(isNotEmpty(mappe))
  require(isNotEmpty(url))
}

class ArtikkelTjeneste(repository: ArtikkelRepository) {

  require(repository != null)

  val kronologiMappe = "chrono"

  private val LOG = LoggerFactory.getLogger(this.getClass)

  def lagreArtikkel(artikkel: Artikkel): String = {
    LOG.debug(s"Lagrer artikkel ${artikkel.id}")
    try {
      TjenesteHjelper.valider(artikkel)
      val endeligArtikkel = sjekkForUfullstendigTittel(artikkel)
      repository.lagreArtikkel(endeligArtikkel)
    } catch {
      case e @ (_: FileNotFoundException | _: IllegalArgumentException) => throw new ClientFaultException(e.getMessage)
      case e: Throwable => throw new ServerFaultException(e.getMessage)
    }
  }

  def slettArtikkel(artikkelId: String) = {
    LOG.debug(s"Sletter artikkel $artikkelId")
    try {
      repository.slettArtikkel(artikkelId)
    } catch {
      case e: FileNotFoundException => throw new NotFoundException(e.getMessage)
      case e: Throwable => throw new ServerFaultException(e.getMessage)
    }
  }

  def hentArtikkel(artikkelId: String): Artikkel = {
    LOG.debug(s"Henter artikkel $artikkelId")
    try {
      repository.hentArtikkel(artikkelId)
    } catch {
      case e: FileNotFoundException => throw new NotFoundException(e.getMessage)
      case e: Throwable => throw new ServerFaultException(e.getMessage)
    }
  }

  def hentArtikkelListe(mappe: String, arkivert: Boolean): List[Artikkel] = {
    LOG.debug(s"Henter artikler fra mappe $mappe")
    try {
      repository.hentArtikler(mappe, arkivert)
    } catch {
      case e @ (_: FileNotFoundException | _: IllegalArgumentException) => throw new ClientFaultException(e.getMessage)
      case e: Throwable => throw new ServerFaultException(e.getMessage)
    }
  }

  def flyttArtikkel(artikkelId: String, nyMappe: Option[String]): Unit = {

    if (nyMappe.isEmpty)
      throw new ClientFaultException("Mappe argument er tomt")

    try {
      LOG.debug("Flytter artikkel til mappe: " + nyMappe.get)
      repository.flyttArtikkel(artikkelId, nyMappe.get)
    } catch {
      case e @ (_: FileNotFoundException | _: IllegalArgumentException) => throw new ClientFaultException(e.getMessage)
      case e: Throwable => throw new ServerFaultException(e.getMessage)
    }
  }
  def arkiverArtikkel(artikkelId: String): Unit = {

    try {
      LOG.debug("Arkiverer artikkel: " + artikkelId)
      repository.arkiverArtikkel(artikkelId)
    } catch {
      case e @ (_: FileNotFoundException | _: IllegalArgumentException) => throw new ClientFaultException(e.getMessage)
      case e: Throwable => throw new ServerFaultException(e.getMessage)
    }
  }

}

object UrlLeser {

  private val LOG = LoggerFactory.getLogger(this.getClass)

  def sjekkForUfullstendigTittel(artikkel: Artikkel) = {
    if (artikkel.tittel == artikkel.url) {
      val hentetTittel = hentTittelFraNettside(artikkel.url)
      artikkel.copy(tittel = hentetTittel)
    } else {
      artikkel
    }
  }

  //  def hentNettside(artikkel: Artikkel) = {
  def hentTittelFraNettside(adresse: String) = {

    try {
    import java.io.BufferedReader

    val url = new URL(adresse)
    val con = url.openConnection
    var encoding = con.getContentEncoding // ** WRONG: should use "con.getContentType()" instead but it returns something like "text/html; charset=UTF-8" so this value must be parsed to extract the actual encoding
    encoding = if (encoding == null) "UTF-8"
               else encoding
//    val in = new BufferedReader(new InputStreamReader(url.openStream, encoding))
//    in.lines.forEach( println )

    val body = Source.fromInputStream(url.openStream(), encoding).mkString
//    println(body)

//    val sb = new StringBuilder(33000)
//    val carr = new Array[Char](4096)
//    var
//    while ()
//    val readLen = in.read(carr, 0, 4096)
//    println("readlen: " + readLen)
//    val body = carr.mkString
//
//    val i = body.indexOf("<title>")
//    println(body.substring(i, 100))

//        println(body)
//
//
//      println
//      println
//      println("===========================================")
//      println
//      println


//    body.matches("*<title>*")

//    val titlePattern = """.*<title>.*""".r
//    val titlePattern = """.*(<title>(\p{Print})</title>).*""".r
    val titlePattern = ".*<title.*>(.*)</title>.*".r


    //    val tittelStreng = titlePattern.findFirstIn(body)
//    val tittelStreng = titlePattern.findFirstIn(body) match { case }

    val tittelStreng = titlePattern.findFirstMatchIn(body) match {
      case Some(i) => i.group(1)
      case None => adresse
    }

//    println(tittelStreng)
    tittelStreng
    } catch {
      case e: Throwable => {
        LOG.error("Feil ved henting av tittel fra nettside. " + e.getMessage)
        LOG.debug(e.getStackTrace.mkString("\n"))
        adresse
      }
    }
  }
}

object TjenesteHjelper {

  def valider(artikkel: Artikkel): Unit = {
    if (isEmpty(artikkel.mappe) || isEmpty(artikkel.tittel) || isEmpty(artikkel.url))
      throw new ClientFaultException("Påkrevde felter mangler: mappe, tittel, eller url")
  }

  def isEmpty(str: String) = str == null || str.trim.isEmpty

  def isNotEmpty(str: String) = !isEmpty(str)
}
