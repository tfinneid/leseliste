package felles

import no.leseliste.felles.WhitelistedChars
import org.scalatest.FlatSpec

class Iso8859_1_Spec extends FlatSpec {

  "En Streng" should "kun støtte hvitlistede tegn" in {

    val lovligeTegn = "nsdlfkjlskdf!23523409 §#=%(^@kh:-%&)AZØÆÅazøæå"

    assert(WhitelistedChars.lovlig_ISO8859_1_Streng(lovligeTegn))
  }

  it should "ikke støtte svartelistede tegn - ´" in {

    val ulovligTegn = "nsdlfkjlskdf!23523409 §#=%(^@kh:-%&)AZØÆÅazøæå´"

    assert(!WhitelistedChars.lovlig_ISO8859_1_Streng(ulovligTegn))
  }
  it should "ikke støtte svartelistede tegn - `" in {

    val ulovligTegn = "nsdlfkjlskdf!23523409 §#=%(^@kh:-%&)AZØÆÅazøæå`"

    assert(!WhitelistedChars.lovlig_ISO8859_1_Streng(ulovligTegn))
  }
  it should "ikke støtte svartelistede tegn - ;" in {

    val ulovligTegn = "nsdlfkjlskdf!23523409 §#=%(^@kh:-%&)AZØÆÅazøæå;"

    assert(!WhitelistedChars.lovlig_ISO8859_1_Streng(ulovligTegn))
  }
}
