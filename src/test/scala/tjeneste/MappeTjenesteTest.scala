package tjeneste

import java.util.Date

import no.leseliste.api.exceptions.ClientFaultException
import no.leseliste.tjeneste.mappe.{MappeRepository, MappeTjeneste}
import org.scalatest.{BeforeAndAfterAll, FlatSpec}
import util.TestHelper

class MappeTjenesteTest extends FlatSpec with BeforeAndAfterAll {

  val mappeRepo = new MappeRepository("./data/test-data")
  val mappetjeneste = new MappeTjeneste(mappeRepo)

  override def beforeAll(): Unit = {
    TestHelper.tømRepo
  }

  "En mappetjeneste" should "ikke ha doble mappenavn" in {

    val mappenavn = s"testmappe-${new Date().getTime}"

    if (!mappetjeneste.hentMappeListe.contains(mappenavn))
      assert(mappetjeneste.opprettMappe(mappenavn))

    assertThrows[ClientFaultException](mappetjeneste.opprettMappe(mappenavn))
  }

  it should "ikke opprette mapper med uten navn" in {

    assertThrows[ClientFaultException](mappetjeneste.opprettMappe(""))
  }

  it should "ikke opprette mapper med ulovlige tegn" in {

    assertThrows[ClientFaultException](mappetjeneste.opprettMappe("\""))
  }

  it should "ikke slette mapper som ikke eksisterer" in {

    assertThrows[ClientFaultException](mappetjeneste.slettMappe("ikkeeksisterendemappe"))
  }

  it should "ikke tømme mapper som ikke eksisterer" in {

    assertThrows[ClientFaultException](mappetjeneste.tømMappe("ikkeeksisterendemappe"))
  }

}
