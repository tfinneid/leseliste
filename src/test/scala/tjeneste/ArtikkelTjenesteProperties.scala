package tjeneste

import java.io.{File, FileNotFoundException}

import no.leseliste.tjeneste.artikkel.{Artikkel, ArtikkelRepository, ArtikkelTjeneste}
import org.scalacheck.Prop.forAll
import org.scalacheck.{Properties, Test}

object ArtikkelTjenesteProperties extends Properties("Artikkelliste") {

  val numberOfTests = 1000 // 5K tests
  override def overrideParameters(p: Test.Parameters): Test.Parameters = p.withMinSuccessfulTests(numberOfTests)

  println(s"Starting ArtikkelTjeneste tests, $numberOfTests rounds of succesful tests...")

  val repo = new ArtikkelRepository("./data/test-data")
  val artikkelTjeneste = new ArtikkelTjeneste(repo)

  val artikkelMappeNavn = "test-artikler"
  val artikkelMappe = new File(s"${repo.sti}/$artikkelMappeNavn/")

  if (!artikkelMappe.exists) {
    artikkelMappe.mkdir()
  }
  if (!artikkelMappe.listFiles.isEmpty) {
    artikkelMappe.listFiles.foreach(_.delete)
  }

  val ikkeArkivertArtikkel = false
  
  /**
    * Artikkellistens integritet
    */

  property("erEnStørre") = forAll {
    (tittel: String, mappe: String, url: String) => { // listen er alltid 1 større en forrige ved hentListe

      val nyArtikkel = Artikkel(tittel + " aaa", artikkelMappeNavn, url + " aaa")

      val artikkelListeFør = artikkelTjeneste.hentArtikkelListe(artikkelMappeNavn, ikkeArkivertArtikkel)
      val nyArtikkelId = artikkelTjeneste.lagreArtikkel(nyArtikkel)
      val artikkelListeEtter = artikkelTjeneste.hentArtikkelListe(artikkelMappeNavn, ikkeArkivertArtikkel)

      if (artikkelListeEtter.length > 20) artikkelMappe.listFiles.foreach(_.delete) // opprydding

      artikkelListeEtter.length == artikkelListeFør.length + 1
    }
  }

  property("erEnMindre") = forAll { (tittel: String, mappe: String, url: String) => // listen er alltid 1 mindre enn forrige ved hentListe

    val eksisterendeArtikkel = Artikkel(tittel + " aaa", artikkelMappeNavn, url + " aaa")
    val eksisterendeArtikkelId = artikkelTjeneste.lagreArtikkel(eksisterendeArtikkel)

    val artikkelListeFørSletting = artikkelTjeneste.hentArtikkelListe(artikkelMappeNavn, ikkeArkivertArtikkel)
    artikkelTjeneste.slettArtikkel(eksisterendeArtikkelId)
    val artikkelListeEtterSletting = artikkelTjeneste.hentArtikkelListe(artikkelMappeNavn, ikkeArkivertArtikkel)

    artikkelListeEtterSletting.length == artikkelListeFørSletting.length - 1
  }

  /**
    * Artikkelmappens integritet
    */

  property("enLagretArtikkelKanHentes") = forAll {
    (tittel: String, mappe: String, url: String) => {
      val lagretArtikkel = Artikkel(tittel + " aaa", artikkelMappeNavn, url + " aaa")
      val lagretArtikkelId = artikkelTjeneste.lagreArtikkel(lagretArtikkel)

      val hentetArtikkel: Artikkel = artikkelTjeneste.hentArtikkel(lagretArtikkelId)
      artikkelTjeneste.slettArtikkel(lagretArtikkelId)

      hentetArtikkel == lagretArtikkel
    }
  }

  property("enLagretArtikkelErBlittSlettet") = forAll { (tittel: String, mappe: String, url: String) =>
    val lagretArtikkel = Artikkel(tittel + " aaa", artikkelMappeNavn, url + " aaa")
    val lagretArtikkelId = artikkelTjeneste.lagreArtikkel(lagretArtikkel)

    val hentetArtikkel: Artikkel = artikkelTjeneste.hentArtikkel(lagretArtikkelId)
    val artikkelEksistererFør = hentetArtikkel == lagretArtikkel

    val artikkelErSlettet = artikkelTjeneste.slettArtikkel(lagretArtikkelId)

    val ex = try {
      artikkelTjeneste.hentArtikkel(lagretArtikkelId)
    } catch {
      case t: FileNotFoundException => t
      case _: Throwable => new Throwable
    }

    artikkelEksistererFør && artikkelErSlettet && ex.isInstanceOf[FileNotFoundException]
  }
}
