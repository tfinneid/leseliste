package tjeneste

import no.leseliste.tjeneste.artikkel.TjenesteHjelper._
import no.leseliste.tjeneste.artikkel.{Artikkel, ArtikkelRepository, ArtikkelTjeneste}
import org.scalacheck.Prop._
import org.scalacheck.{Properties, Test}

object ArtikkelInitProperties extends Properties("Artikkel") {

  val numberOfTests = 50000 // 5M tests
  override def overrideParameters(p: Test.Parameters): Test.Parameters = p.withMinSuccessfulTests(numberOfTests)

  println(s"Starting ArtikkelInit tests, $numberOfTests rounds of succesful tests...")

  property("init") = forAll {
    (tittel: String, mappe: String, url: String) => {

      lazy val artikkel = Artikkel(tittel, mappe, url)

      if (isEmpty(tittel) || isEmpty(mappe) || isEmpty(url)) {
        throws(classOf[IllegalArgumentException]) { artikkel }
      } else {
        val genId = artikkel.id.split(",")

        genId(0).toLong == artikkel.tidsstempel && isNotEmpty(genId(1)) && genId(1).length == 45 &&
          artikkel.tittel == tittel &&
          artikkel.mappe == mappe &&
          artikkel.url == url
      }
    }
  }
}
