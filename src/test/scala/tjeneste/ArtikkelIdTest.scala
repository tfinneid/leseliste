package tjeneste

import java.util.Date

import no.leseliste.tjeneste.artikkel.Artikkel
import no.leseliste.tjeneste.artikkel.TjenesteHjelper.isNotEmpty
import org.scalatest.FlatSpec

class ArtikkelIdTest extends FlatSpec {

  val testIterasjoner = 50000 //00

  val idMap = scala.collection.mutable.SortedSet[String]()

  "En Artikkel" should "ha unik id" in {

    for (i <- 1 to testIterasjoner) {
      val artikkel = Artikkel("aaa", "bbb", "ccc")

      val containsUnique = !idMap.contains(artikkel.id)
      val idUnique = idMap.add(artikkel.id)

      assert(containsUnique, s"containsUnique feilet etter $i antall tester")
      assert(idUnique, s"idUnique feilet etter $i antall tester")
    }
  }

  it should "ha gyldig tidsstempel" in {

    for (i <- 1 to testIterasjoner) {
      val ts = new Date().getTime

      val artikkel = Artikkel("aaa", "bbb", "ccc")

      assert(artikkel.tidsstempel >= ts)
    }
  }

  it should "ha gyldig id" in {

    val sekvensstrengLengde = 45 // alfanumerisk == 62^45 tegn == 10^80 == >2^256

    for (i <- 1 to testIterasjoner) {
      val artikkel = Artikkel("aaa", "bbb", "ccc")

      val genId = artikkel.id.split(",")

      assert(genId(0).toLong == artikkel.tidsstempel, "Tidsstempel stemmer ikke med id sin tidsstempedel")
      assert(isNotEmpty(genId(1)), "Sekvensstreng er tom.")
      assert(genId(1).length == sekvensstrengLengde, s"Sekvensstreng er ikke $sekvensstrengLengde tegn lang")
    }
  }
}
