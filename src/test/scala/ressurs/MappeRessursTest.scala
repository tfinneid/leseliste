package ressurs

import java.net.URL
import java.util.Date

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import com.mashape.unirest.http.{HttpResponse, Unirest}
import no.leseliste.api.mappe.MappeRessurs
import no.leseliste.tjeneste.mappe.{MappeRepository, MappeTjeneste}
import org.scalatest.{BeforeAndAfterAll, FlatSpec}
import org.slf4j.LoggerFactory
import spark.Spark
import util.TestHelper

class MappeRessursTest extends FlatSpec with BeforeAndAfterAll {

  private val LOG = LoggerFactory.getLogger(this.getClass)

  val jsonMapper = new ObjectMapper() with ScalaObjectMapper
  jsonMapper.registerModule(DefaultScalaModule)
  jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

  val mappeRepo = new MappeRepository("./data/test-data")
  val mappetjeneste = new MappeTjeneste(mappeRepo)
  val mappeRessurs = new MappeRessurs(mappetjeneste, jsonMapper)


  override def beforeAll(): Unit = {
    LOG.info("Starter leseliste test-rammeverk")
    TestHelper.tømRepo
    Spark.init()
    mappeRessurs.start
    Thread.sleep(3000)

  }

  override def afterAll(): Unit = {
    LOG.info("Stopper leseliste test-rammeverk")
    Spark.stop()
    Thread.sleep(3000)
  }

  val mappesti = "http://localhost:4567/api/folders"
  val mappenavn = s"testmappe-${new Date().getTime}"
  val url = new URL(s"$mappesti/$mappenavn")
  val nyMappe = s"ny-testmappe-${new Date().getTime}"
  val nyUrl = new URL(s"$mappesti/$nyMappe")

  // TODO kjøres tester alltid samme rekkefølge som i koden? dvs blir mappen alltid opprettet først.

  "En mappe" should "kunne opprettes" in {
    assert( ! getFolder(mappesti).contains(mappenavn) )

    val res = Unirest.post(url.toString).asString
    assert(res.getStatus == 201)
    assert(locationHeader(res) == url.toString)

    assert( getFolder(mappesti).contains(mappenavn) )
  }

  it should "kunne hentes" in {
    assert( getFolder(mappesti).contains(mappenavn) )
  }

  it should "kunne endre navn" in {
    assert( getFolder(mappesti).contains(mappenavn) )
    assert( ! getFolder(mappesti).contains(nyMappe) )

    val res = Unirest.put(url.toString)
        .queryString("newname", nyMappe)
        .asString
    assert(res.getStatus == 204)
    assert(locationHeader(res) == url.toString)

    assert( ! getFolder(mappesti).contains(mappenavn) )
    assert( getFolder(mappesti).contains(nyMappe) )
  }

  it should "kunne slettes" in {
    assert( getFolder(mappesti).contains(nyMappe) )

    val res = Unirest.delete(nyUrl.toString).asJson
    assert(res.getStatus == 204)

    assert( ! getFolder(mappesti).contains(nyMappe) )
  }

  // TODO må teste å tømme mappe også

  private def locationHeader(res: HttpResponse[String]) = res.getHeaders.get("Location").get(0).stripSuffix("/n")

  private def getFolder(mappesti: String): Array[String] = {
    val res = Unirest.get(mappesti)
    res.asJson().getBody.toString.replace("[\"", "").replace("\"]", "").replace("\",\"", ",").split(",")
  }
}
