package ressurs

import java.net.URL
import java.util.Date

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import com.mashape.unirest.http.Unirest
import no.leseliste.api.mappe.MappeRessurs
import no.leseliste.tjeneste.mappe.{MappeRepository, MappeTjeneste}
import org.scalatest.{BeforeAndAfterAll, FlatSpec}
import org.slf4j.LoggerFactory
import spark.Spark
import util.TestHelper

class MappeRessursFeilhåndteirngsTest extends FlatSpec with BeforeAndAfterAll {

  private val LOG = LoggerFactory.getLogger(this.getClass)

  val jsonMapper = new ObjectMapper() with ScalaObjectMapper
  jsonMapper.registerModule(DefaultScalaModule)
  jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

  val mappeRepo = new MappeRepository("./data/test-data")
  val mappetjeneste = new MappeTjeneste(mappeRepo)
  val mappeRessurs = new MappeRessurs(mappetjeneste, jsonMapper)


  override def beforeAll(): Unit = {
    LOG.info("Starter leseliste test-rammeverk")
    TestHelper.tømRepo
    Spark.init()
    mappeRessurs.start
    Thread.sleep(3000)

  }

  override def afterAll(): Unit = {
    LOG.info("Stopper leseliste test-rammeverk")
    Spark.stop()
    Thread.sleep(3000)
  }

  val mappesti = "http://localhost:4567/api/folders"
  val mappenavn = s"testmappe-${new Date().getTime}"
  val url = new URL(s"$mappesti/$mappenavn")
  val nyMappe = s"ny-testmappe-${new Date().getTime}"
  val nyUrl = new URL(s"$mappesti/$nyMappe")

  /** TODO Må teste at ressursens logikk er korrekt, mhp
    *      - request parsing (url og parameter/body tolkning)
    *      - håndtering/mapping av feilsituasjoner til http feilkoder
    *      - korrekt respons, headers og body
    *
    * - At de forskjellige typer feil faktisk mappes til riktig http feil kode
    *   - spesifikke underliggende feil situasjoner er allerede testet, så type exception trengs bare mappes riktig
    * - At response eventuelt inneholde Location header, samt korrekt status kode
    *   - Trenger bare å forholde seg til de statuskoder koden selv genererer, og eventuelle andre tekniske situasjoner
    *     som er en sentral del av API'et
    */

  "En mappes feilhåndtering" should "ikke tillate opprettelse av eksisterende mappe" in {
    assert(Unirest.post(url.toString).asString.getStatus == 201)

    val res = Unirest.post(url.toString).asString
    assert(res.getStatus == 400)
  }

  it should "ikke tillate sletting av ikkeeksisterende mapper" in {

    val res = Unirest.delete(url.toString+"-ukjentmappe").asString
    assert(res.getStatus == 400)
  }

  it should "ikke endre mappenavn til et tomt navn" in {

    val res = Unirest.post(new URL(mappesti).toString).asString
    assert(res.getStatus == 404)
  }

  it should "ikke endre mappenavn til et tomt navn med skråstrek" in {

    val res = Unirest.post(new URL(mappesti+"/").toString).asString
    assert(res.getStatus == 404)
  }
}
