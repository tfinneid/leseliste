package repository

import java.util.Date

import no.leseliste.tjeneste.mappe.MappeRepository
import org.scalatest.{BeforeAndAfterAll, FlatSpec}
import util.TestHelper

class MappeRepositoryTest extends FlatSpec with BeforeAndAfterAll {

  val mappeRepo = new MappeRepository("./data/test-data")

  override def beforeAll(): Unit = {
    TestHelper.tømRepo
  }

  "En mapperepository" should "ikke ha doble mappenavn" in {

    val mappenavn = s"testmappe-${new Date().getTime}"

    if (!mappeRepo.hentMappeListe.contains(mappenavn))
      assert(mappeRepo.opprettMappe(mappenavn))

    assertThrows[IllegalArgumentException](mappeRepo.opprettMappe(mappenavn))
  }

  it should "ikke opprette mapper med uten navn" in {

    assertThrows[IllegalArgumentException](mappeRepo.opprettMappe(""))
  }

  it should "ikke opprette mapper med ulovlige tegn" in {

    assertThrows[IllegalArgumentException](mappeRepo.opprettMappe("\""))
  }

  it should "ikke slette mapper som ikke eksisterer" in {

    assertThrows[IllegalArgumentException](mappeRepo.slettMappe("ikkeeksisterendemappe"))
  }

  it should "ikke tømme mapper som ikke eksisterer" in {

    assertThrows[IllegalArgumentException](mappeRepo.tømMappe("ikkeeksisterendemappe"))
  }

}
