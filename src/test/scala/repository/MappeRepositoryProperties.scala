package repository

import java.io.File

import no.leseliste.tjeneste.artikkel.TjenesteHjelper._
import no.leseliste.tjeneste.mappe.MappeRepository
import org.scalacheck.Gen._
import org.scalacheck.Prop._
import org.scalacheck.{Properties, Test}
import util.TestHelper.tømRepo

// TODO: !!!!!  Kjører fint i intellij, men feiler i SBT !!!!!
/**
  *  En og en test kjører fint i sbt, men ikke når alle tester kan kjøres.
  *  Det er destruktive operasjoner som , tømRepo(), og tester som slett og tøm, som skaper problemer.
  *  Det kan virke som om flere tester kjører samtidig i samme datakatalog
  *  Noe av problemet kan også være fordi ScalaCheck kjører separat fra ScalaTest
  */

object MappeRepositoryProperties extends Properties("Mappe") {

  println("\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n!!!!!   MappeRepositoryProperties feiler i SBT   !!!!!\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n")

  override def overrideParameters(p: Test.Parameters): Test.Parameters = p.withMinSuccessfulTests(10000) // 10K tests)

  val repo = new MappeRepository("./data/test-data")
  val repoFil = new File(s"${repo.sti}")

  val lovligeTegn = """([\x20-\x7E\xA1-\xFF&&[^\x22\x27\x7E\x5C\x7C\xB4\x60\x3B\x24\x2E\x2F\x5B\x5D\x7B\x7D\xAD]]*)""".r


  def lovlig_ISO8859_1_mappenavn(str: String) = str match {
    case lovligeTegn(s) => true
    case _ => false
  }

  tømRepo  // Hindrer muligens å avsløre feil fordi repo er alltid tomt før hver eneste argument kjøres...

  property("opprett") = {
    forAll(asciiStr) {
      (navn: String) => {

        val mappe = new File(repoFil, navn)

        lazy val opprettMappeOperasjon = repo.opprettMappe(navn)

        if (isEmpty(navn)) throws(classOf[IllegalArgumentException]) { opprettMappeOperasjon }
        else if (!lovlig_ISO8859_1_mappenavn(navn)) throws(classOf[IllegalArgumentException]) { opprettMappeOperasjon }
        // Mappe til aldri eksistere fordi testen tømmer repo
        else if (mappe.exists) throws(classOf[IllegalArgumentException]) { opprettMappeOperasjon }
        else {
          opprettMappeOperasjon && mappe.exists
        }
      }
    }
  }

  property("slett") = forAll(asciiStr) {
    tømRepo
    (navn: String) => {

      val mappe = new File(repoFil, navn)

      lazy val opprettMappe = repo.opprettMappe(navn)
      lazy val slettMappe = repo.slettMappe(navn)

      if (isEmpty(navn)) throws(classOf[IllegalArgumentException]) { slettMappe }
      else if (!lovlig_ISO8859_1_mappenavn(navn)) throws(classOf[IllegalArgumentException]) { slettMappe }
      else {
        !mappe.exists && opprettMappe && mappe.exists &&
          slettMappe && !mappe.exists
      }
    }
  }

  property("toem") = forAll(asciiStr) {
    tømRepo
    (navn: String) => {

      val mappe = new File(repoFil, navn)

      lazy val opprettMappe = repo.opprettMappe(navn)
      lazy val tømMappe = repo.tømMappe(navn)

      if (isEmpty(navn)) throws(classOf[IllegalArgumentException]) { tømMappe }
      else if (!lovlig_ISO8859_1_mappenavn(navn)) throws(classOf[IllegalArgumentException]) { tømMappe }
      else {
        if (!mappe.exists)
          opprettMappe
        val mappeExisterer = mappe.exists
        val fil1Opprettet = new File(mappe, "testfil1").createNewFile
        val fil2Opprettet = new File(mappe, "testfil2").createNewFile
        val fil3Opprettet = new File(mappe, "testfil3").createNewFile

        val tømmingUtført = tømMappe
        val mappeFinnesEtterTømming = mappe.exists
        val mappeTom = mappe.listFiles.length == 0
        mappe.delete

        mappeExisterer && fil1Opprettet && fil2Opprettet && fil3Opprettet &&
          tømmingUtført && mappeFinnesEtterTømming && mappeTom
      }
    }
  }

  property("endreNavn") = forAll(asciiStr, asciiStr) {
    tømRepo
    (navn: String, nyttNavn: String) => {

      val mappe = new File(repoFil, navn)
      val nyMappe = new File(repoFil, nyttNavn)

      lazy val opprettMappe = repo.opprettMappe(navn)
      lazy val endreMappeNavn = repo.endreMappenavn(navn, nyttNavn)

      if (isEmpty(navn) || isEmpty(nyttNavn)) throws(classOf[IllegalArgumentException]) { endreMappeNavn }
      else if (!lovlig_ISO8859_1_mappenavn(navn) || !lovlig_ISO8859_1_mappenavn(nyttNavn))
          throws(classOf[IllegalArgumentException]) { endreMappeNavn }
//      else if (!mappe.exists || nyMappe.exists) throws(classOf[IllegalArgumentException]) { endreMappeNavn }  // Hindrer testen uten endringer
      else if (navn == nyttNavn) throws(classOf[IllegalArgumentException]) { endreMappeNavn }
      else {
        val mappeOpprettet = opprettMappe

        val mappenavnEndret = endreMappeNavn
        val nyMappeEksisterer = nyMappe.exists
        val gammelMappeBorte = !mappe.exists

        mappe.delete
        nyMappe.delete

        mappeOpprettet && mappenavnEndret && nyMappeEksisterer && gammelMappeBorte
      }
    }
  }

  property("hentListe") = forAll(listOf(alphaStr)) { // Bruker alphaStr fordi repo operasjonen ikke har bruker argumenter
    tømRepo
    (mappernavn: List[String]) => {

      val filliste = mappernavn
        .filter(isNotEmpty)
        .map(f => new File(s"$repoFil/$f"))
        .filter(_.mkdir)

      val mappeliste = repo.hentMappeListe

      filliste.foreach(_.delete)

      mappeliste.diff(filliste.map(_.getName)).size == 0
    }
  }
}
