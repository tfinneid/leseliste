package util

import java.io.File

import repository.MappeRepositoryProperties.repo

object TestHelper {

  val katalogFeilmelding = "\n\n################################################\n#####   test-data katalogen er ikke tom   ######\n################################################\n"

  def tømRepo {
    val repoFil = new File(s"${repo.sti}")

    def deleteRecursively(file: File): Unit = {
      if (file.isDirectory)
        file.listFiles.foreach(deleteRecursively)
      if (file.exists && !file.delete)
        throw new java.io.IOException(s"Unable to delete ${file.getAbsolutePath}")
    }

    if (!repoFil.listFiles.isEmpty) {
      repoFil.listFiles.foreach( f => {
        if (f.isDirectory) deleteRecursively(f)
        else f.delete()
      })
    }

    assert(repoFil.listFiles.length == 0L, katalogFeilmelding )
  }

}
