name := "leseliste"
version := readVersionString

scalaVersion := "2.12.13"
cancelable in Global := true
scalaVersion in ThisBuild := "2.12.13"

// *** Prosjektplugins som krever er dokumentert nederst i filen  ****

enablePlugins(PackPlugin)

mainClass in (Compile,run) := Some("no.leseliste.Leseliste")
packageOptions in (Compile, packageBin) += Package.ManifestAttributes( "Main-Class" -> "no.leseliste.Leseliste" )

// Pack plugin config

packMain := Map("leseliste" -> "no.leseliste.Leseliste")
packGenerateWindowsBatFile := false
packResourceDir += (baseDirectory.value / "src/main/public" -> "public/")
packResourceDir += (baseDirectory.value / "src/main/conf" -> "conf/")
packResourceDir += (baseDirectory.value / "src/main/bin" -> "bin/")

resourceFiles := Seq(
  (baseDirectory.value / "README.txt" -> ""),
  (baseDirectory.value / "version.txt" -> "")
)

// Platform dependencies

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-reflect" % "2.12.13",
  "org.scala-lang.modules" % "scala-xml_2.12" % "1.3.0"
)

// Application Dependencies

//val jetty-version: String = "9.4.37.v20210219"

libraryDependencies += "com.sparkjava" % "spark-core" % "2.9.3"

// TODO: Overstyrer spark avhengigheter, senere versjon av Spark kan ha oppgradert jetty versjonen 
libraryDependencies += "org.eclipse.jetty" % "jetty-server" % "9.4.37.v20210219"
libraryDependencies += "org.eclipse.jetty" % "jetty-webapp" % "9.4.37.v20210219"
libraryDependencies += "org.eclipse.jetty.websocket" % "websocket-server" % "9.4.37.v20210219"
libraryDependencies += "org.eclipse.jetty.websocket" % "websocket-servlet" % "9.4.37.v20210219"

libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.12.1"
libraryDependencies += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.12.1"

//libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.25"
libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "1.7.30"
// log4j 2 binding for slf4j - ikke testet enda
//libraryDependencies += "org.apache.logging.log4j" % "log4j-slf4j-impl" % "2.9.1"

libraryDependencies += "io.dropwizard.metrics" % "metrics-core" % "4.1.17"
libraryDependencies += "io.dropwizard.metrics" % "metrics-servlets" % "4.1.17"

// Test depenencies

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.9" % "test"
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.3" % "test"

libraryDependencies += "com.mashape.unirest" % "unirest-java" % "1.4.9" % "test"


//dependencyCheckOutputDirectory := "/Users/tofi/src/leseliste/"



// project/plugins.sbt config
//
// addSbtPlugin("org.xerial.sbt" % "sbt-pack" % "0.13")
// addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.5.1")
// addSbtPlugin("net.vonbuchholtz" % "sbt-dependency-check" % "3.1.1")
// addSbtPlugin("com.github.cb372" % "sbt-explicit-dependencies" % "0.2.16")
// addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.10.0-RC1")




// Custom tasks

lazy val resourceFiles = settingKey[Seq[Tuple2[java.io.File, String]]]("Sti til filer som skal kopieres")
lazy val dist = taskKey[Unit]("Bygger med Pack og samler ekstra filer til target/pack")

dist := {
  val logger: Logger = sLog.value
  val packs = pack.value

  val otherResources = resourceFiles.value

  otherResources.foreach( otherFiles => {
    val srcFile = otherFiles._1
    val destDir = otherFiles._2
    val fileName = otherFiles._1.name
    val destFile = packTargetDir.value / packDir.value / destDir / fileName

    logger.info(s"Kopierer $srcFile til $destFile ")
    IO.copyFile(srcFile, destFile)
  })
}

def readVersionString: String = {
  import java.io.FileInputStream
  import scala.io.Source

  val in = new FileInputStream( "version.txt" )
  val f = Source.fromInputStream(in)
  val v = f.getLines.mkString
  f.close
  v
}
