
Trenger miljøvariabler og katalog for data og konfigurasjon

Kortinstruksjon
- opprett data katalog
- kopier oppstart- og konfig-filer til datakatalogen
- rediger oppstartsfilen for riktige miljøvariabler
- start leseliste applikasjonene

Applikasjonen kan nås Via en nettleser på http://[LL_NETWORK_ADRESSEN]:6877/


Detaljert instruksjon
- opprett katalog $HOME/lesedata/bin og conf
- Kopier DIST/bin/leseliste_start.sh.template til lesedata/bin/leseliste_start.sh
  - Rediger variablene INSTALL_DIR, LL_MODE, LL_NETWORK og LL_PORT
    - INSTALL_DIR er komplett sti applikasjonskatalogen leseliste-x.y.z/
    - LL_MODE kan inneholde verdiene "production" eller "development"
- Kopier DIST/conf/log4j.properties.template til lesedata/conf/log4j.properties og rediger hvis nødvendig
  - loggfilene legges automatisk i lesedata/logg, uavhengig om detn konfigureres i log4j filen.


Redigering av leseliste_start.sh malen

- Produksjons modus innebærer at webserveren er konfigurert med enkelte verdier egnet for kjøring på offentlig server,
  mens i utviklingsmodus har disse verdiene litt friere innstillinger for å hjelpe utvikler i arbeidet. 

- production mode henter konfig og data fra lesedata katalogen
- development mode henter konfig og data fra katalogen applikasjonen starter i,
      som ofte er fra PROJ/src/main/conf og data fra PROJ/lesedata ???
 




Utvikler notater

modus detaljer
- utvikling
  - server starter kun på loopback nettverksgrensesnitt, men portnr kan endres
  - artifakter og loggine hentes fra prosjektkatalogene, eks [SRCPATH]/src/main/public eller [SRCPATH]/lesedata/data
  - CORS modus er støttet
  - detaljert forespørselslogging er skrudd på
  
- prod
  - loopback netverksgrensesnitt er ikke støttet
  - webartifakter hentes fra public/ katalogen i oppstartskatalogen 
  - CORS mode er ikke støttet


