#!/bin/bash

if hg st | grep -i version.txt > /dev/null ; then
    echo "version.txt already edited."
    exit 1;
else
    version=$(cat version.txt)

    semver=( ${version//./ } )
    major="${semver[0]}"
    minor="${semver[1]}"
    patch="${semver[2]}"

    ((patch++))

    echo "${major}.${minor}.${patch}" > version.txt
fi

