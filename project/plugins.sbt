
addSbtPlugin("org.xerial.sbt" % "sbt-pack" % "0.13")  
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.5.1")
addSbtPlugin("net.vonbuchholtz" % "sbt-dependency-check" % "3.1.1")
addSbtPlugin("com.github.cb372" % "sbt-explicit-dependencies" % "0.2.16")
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.10.0-RC1")
